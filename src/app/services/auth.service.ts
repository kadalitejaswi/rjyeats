import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _loginUrl = 'http://localhost:3000/api/user/login'
  private _registerUrl = 'http://localhost:3000/api/user/register'

  public token;
  public btoken= new BehaviorSubject('');

  constructor(private http:HttpClient , private router:Router) { }

  userRegister(userdata){
    return this.http.post<any>(this._registerUrl,userdata);

  }

userToken(token){
  this.token=token;
}

  userLogin(user){
    return this.http.post<any>(this._loginUrl,user)
  }



  logedIn(){
    return !!localStorage.getItem('token');
  }
  getToken(){
    return localStorage.getItem('token')
  }
  logedout(){
    localStorage.removeItem('token')
    this.router.navigate(['/home'])
  }
}
