import { Injectable, Injector } from '@angular/core';
import { AuthService } from "./auth.service";
import { HttpInterceptor } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptService implements HttpInterceptor {

  constructor( private injector:Injector) { }

  intercept(req,next){
    let auth=this.injector.get(AuthService);
    let tokenizedReq=req.clone({ 
      setHeaders:{
        'x-access-token':`${auth.getToken()}`
      }
    });
    return next.handle(tokenizedReq)
  }
}
