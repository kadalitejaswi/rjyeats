import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerdata={}

  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit() {
  } 
  registerUser(){
    console.log(this.registerdata);
    
      this.auth.userRegister(this.registerdata).subscribe(res=>{
        console.log(res);
        if(res.auth){
          localStorage.setItem('token',res.token)
          this.router.navigate(['/sweetGallery'])
        }else{
          this.router.navigate(['/login'])
          
        }
        
  
        
      }),error=>{
        console.log(error);
        
        
      }
    }
}

