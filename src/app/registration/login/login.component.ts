import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
login={}
error=''
  constructor( private auth:AuthService , private router:Router) { }

  ngOnInit() {
  }

  
  loginData(){
    console.log(this.login);
    
      this.auth.userLogin(this.login).subscribe(res=>{
        console.log(res);
        this.auth.userToken=res.token;
        this.auth.btoken.next(res.token)
        let value=this.auth.btoken.getValue()
        console.log(value);
        
        if(res.auth){
          localStorage.setItem('token',res.token);
          this.router.navigate(['/sweetGallery']);
         }else{
        this.router.navigate(['/home']);
  }
  
        
      }),err=>{
        console.log(err);
        
        this.error = err.error.message;   
      }
    }



}
