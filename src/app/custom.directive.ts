import { Directive , ElementRef , Renderer, HostListener, HostBinding} from '@angular/core';

@Directive({
  selector: '[appCustom]'
})
export class CustomDirective {

  constructor(private el:ElementRef , private re:Renderer) {
    el.nativeElement.style.color="pink"
  }

     @HostListener("click") onclick()
     {
    alert("clicked")
  }
    @HostListener("mouseleave") mouse()
  {
    this.changeColor("red")
  }
  changeColor(color:string){
    this.re.setElementStyle(this.el.nativeElement,'color',color)
  }
}
