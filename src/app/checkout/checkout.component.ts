import { Component, OnInit ,Input , Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
checking=[
          {   sweet:"kova",
              rating:"5star"
          },
          {   sweet:"Laddu",
              rating:"4star"
          },
          {   sweet:"jelly",
              rating:"5star"
          },
        ]

show:Boolean=true

  @Input() private parentData
  constructor() { }
      
  @Output() rating =new EventEmitter() 
  ngOnInit() {
  }


  rateClick(){
    this.show= !this.show
    this.rating.emit(this.checking)
   
    
    }
 
}
