import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sweet-gallery',
  templateUrl: './sweet-gallery.component.html',
  styleUrls: ['./sweet-gallery.component.css']
})
export class SweetGalleryComponent implements OnInit {
public showSweets:boolean=false;
fullImagePath: string;
 sweets=[{
  name:"Laddu",
  cost:200,
  img:this.fullImagePath,
},
{
  name:"Putharekulu",
  cost:300
},
{
  name:"Kova",
  cost:500
}
]


    constructor() {
      this.fullImagePath='../assets/images/Mothichur.jpg'
    }

  public hideSweets(){
    this.showSweets = !this.showSweets; //true
  }

    ngOnInit() {
     
    }
  

  
 
  
  
  }