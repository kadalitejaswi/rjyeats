import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SweetGalleryComponent } from './sweet-gallery.component';

describe('SweetGalleryComponent', () => {
  let component: SweetGalleryComponent;
  let fixture: ComponentFixture<SweetGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SweetGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SweetGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
