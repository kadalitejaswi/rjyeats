import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './registration/login/login.component';
import { RegisterComponent } from './registration/register/register.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientModule, HTTP_INTERCEPTORS  } from "@angular/common/http";
import { AuthService } from './services/auth.service';
import { FormsModule } from "@angular/forms";
import { SweetGalleryComponent } from './sweet-gallery/sweet-gallery.component';
import { AuthGuard  } from "./auth.guard";
import { TokenInterceptService } from './services/token-intercept.service';
import { AddcartComponent } from './addcart/addcart.component';
import { SweetgalleryDirectiveDirective } from './sweetgallery-directive.directive';
import { CustomDirective } from './custom.directive';
import { FilterPipe } from './filter.pipe';
import { CheckoutComponent } from './checkout/checkout.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NavbarComponent,
    SweetGalleryComponent,
    AddcartComponent,
    SweetgalleryDirectiveDirective,
    CustomDirective,
    FilterPipe,
    CheckoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFontAwesomeModule
    
  ],
  providers: [AuthService,AuthGuard,{
  provide:HTTP_INTERCEPTORS,
  useClass:TokenInterceptService,
  multi:true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
