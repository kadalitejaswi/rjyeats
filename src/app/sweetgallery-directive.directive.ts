import { Directive, ElementRef,Renderer, HostListener ,HostBinding } from '@angular/core';

@Directive({
  selector: '[appSweetgalleryDirective]'
})
export class SweetgalleryDirectiveDirective {

  constructor(private el:ElementRef , private re:Renderer) { 

  // el.nativeElement.style.color='brown';
  
  }
 @HostListener("click") onclick(){
   alert("clicked")
 }
 @HostListener("mouseover") mouseO (){
  this.changeColor1('pink')
  this.border="1px solid green"

 }
 changeColor1(color:string){
   this.re.setElementStyle(this.el.nativeElement, 'color' , color)
 }
@HostListener("mouseleave") mouseL (){
  this.changeColor('green')
  this.border="1px solid red"
}
changeColor(Color:string){
  this.re.setElementStyle(this.el.nativeElement,'color', Color)
}
@HostBinding("style.border") border:string
}