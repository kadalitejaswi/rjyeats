import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './registration/register/register.component';
import { LoginComponent } from './registration/login/login.component';
import { HomeComponent } from './home/home.component';
import { SweetGalleryComponent } from './sweet-gallery/sweet-gallery.component';
import { AuthGuard } from './auth.guard';
import { AddcartComponent } from './addcart/addcart.component';

const routes: Routes = [
  {
    path:"register",
    component:RegisterComponent
  },
  { path:"login",
    component:LoginComponent
  },
  {
    path:"home",
    component:HomeComponent},
  { path:"addcart",
    component:AddcartComponent
  },

  // can activate method for guard
  
  { path:"sweetGallery",
    component:SweetGalleryComponent,
    canActivate:[AuthGuard]
  },
  { path:"**",
    component:HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
